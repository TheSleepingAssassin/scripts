#!/usr/bin/env bash

if [ $# -eq 0 ]
then
  xterm -fa 'JetBrains Mono Medium' -fs 13 -e nvim
else
  xterm -fa 'JetBrains Mono Medium' -fs 13 -e nvim $1
fi
